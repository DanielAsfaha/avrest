<?php

namespace Aviatoo\Rest\Constants;


abstract class Enum
{
    /**
     * @return mixed
     */
    public static function getConstants(){
        $reflectionClass = new \ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }
}