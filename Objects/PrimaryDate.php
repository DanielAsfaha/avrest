<?php
namespace Aviatoo\Rest\Objects;




class PrimaryDate extends \DateTime
{
    public function __toString(){
        return $this->format('U');
    }

}