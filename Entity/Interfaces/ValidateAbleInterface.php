<?php
namespace Aviatoo\Rest\Entity\Interfaces;

/**
 * Interface ValidateAbleInterface
 * @package Aviatoo\Rest\Entity\Interfaces
 */
interface ValidateAbleInterface{

    /**
     * @return void
     */
    public function validateInsert() : void;

    /**
     * @return void
     */
    public function validateUpdate() : void;
}