<?php

namespace Aviatoo\Rest\Entity\Traits;


use Aviatoo\Rest\Entity\Article;
use Aviatoo\Rest\Entity\ArticleImage;
use Aviatoo\Rest\Entity\ArticleSize;
use Aviatoo\Rest\Entity\Interfaces\EntityInterface;
use Aviatoo\Rest\Entity\Interfaces\OwnAbleInterface;
use Aviatoo\Rest\Entity\User;

trait OwnAbleTrait
{
    /**
     * @param User $user
     * @return bool
     */
    public function isOwnedBy(User $user){

        if($user->isAdmin())return true;

        $user=$this->getOwner();
        $res=$user->getId() === $user->getId();

        return $res;
    }

}