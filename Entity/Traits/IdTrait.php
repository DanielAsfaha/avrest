<?php
namespace Aviatoo\Rest\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Aviatoo\Rest\Annotation\NotBlank;
use Aviatoo\Rest\Annotation\CustomGroups;

/**
 * Class IdTrait
 * @package Aviatoo\Rest\Entity\Traits
 * This Trait cannot be used for Base classes (ERBUNG GEHT HIER NICHT)
 */
trait IdTrait{

    /**
     * @NotBlank
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @CustomGroups({})
     */
    protected $id;

    /**
     * @return int
     */
    public function getId(){return $this->id;}

    /**
     * @param $id
     * @return $this
     */
    public function setId($id){$this->id=$id;return $this;}
}