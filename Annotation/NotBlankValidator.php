<?php
namespace Aviatoo\Rest\Annotation;
use Aviatoo\Rest\Annotation\Traits\GroupFetcherTrait;
use Doctrine\Common\Annotations\DocParser;
use JMS\Serializer\Annotation\Groups;
use Aviatoo\Rest\Constants\GroupConstants;

/**
 * Class Parameter
 * @package Aviatoo\Rest\Annotation
 */
class NotBlankValidator extends \Symfony\Component\Validator\Constraints\NotBlankValidator
{

}
