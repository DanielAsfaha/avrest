<?php

namespace Aviatoo\Rest\EventListener;

use Aviatoo\Rest\Repository\Pagination;
use Aviatoo\Rest\Controller\BaseController;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class PaginationListener
{
    private $annotationReader;
    public function __construct(Reader $annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controllerAndAction = $event->getController();

        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controllerAndAction)) {
            return;
        }
        $controller = $controllerAndAction[0];
        $action = $controllerAndAction[1];
        $reflectionMethod = new \ReflectionMethod(get_class($controller),$action);
        $annotation = $this->annotationReader->getMethodAnnotation($reflectionMethod,\Aviatoo\Rest\Annotation\Pagination::class);
        if($controller instanceof BaseController &&  $annotation  && $annotation instanceof \Aviatoo\Rest\Annotation\Pagination){

            $page = $event->getRequest()->get("page",0);
            $page_size = $annotation->getPageSize();
            if(is_numeric($page) && is_numeric($page_size)){
                Pagination::buildPagination($page,$page_size);
            }
        }


    }

}