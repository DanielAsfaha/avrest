<?php
namespace Aviatoo\Rest\Exception;


use Aviatoo\Rest\Entity\Interfaces\EntityInterface;
use Aviatoo\Rest\Exception\Base\ApiException;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Class InvalidParamsException
 * @package Aviatoo\Rest\Exception
 */
class InvalidParamsException extends ApiException
{
    const MESSAGE = 'Invalid parameters!';
    const STATUS_CODE = 400;

    /**
     * InvalidParamsException constructor.
     * @param ConstraintViolationListInterface $violationList
     * @param bool $msg
     */
    public function __construct(ConstraintViolationListInterface $violationList,$msg=false) {
        if(!$msg)$msg=self::MESSAGE;
        $errorData = $this->getErrors($violationList);
        parent::__construct(self::STATUS_CODE, $errorData, $msg);
    }

    /**
     * @param ConstraintViolationListInterface $violationList
     * @return mixed
     */
    private function getErrors(ConstraintViolationListInterface $violationList)
    {

        $errors = [];
        /** @var ConstraintViolationInterface $error */

        foreach ($violationList as $error) {
            $errors['validation'][] = [
                "class"=>get_class($error->getRoot()),
                'field' => $error->getPropertyPath(),
                'message' => $error->getMessage()
            ];
        }
        return $errors;
    }


}
