<?php
namespace Aviatoo\Rest\Exception;

use Aviatoo\Rest\Exception\Interfaces\ApiExceptionInterface;


/**
 * Class UnsupportedMediaTypeHttpException
 * @package Aviatoo\Rest\Exception
 */
class UnsupportedMediaTypeHttpException extends \Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException implements ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return parent::getStatusCode();
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return [];
    }


}