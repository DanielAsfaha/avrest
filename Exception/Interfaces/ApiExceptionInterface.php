<?php
namespace Aviatoo\Rest\Exception\Interfaces;

/**
 * Interface ApiExceptionInterface
 * @package Aviatoo\Rest\Exception\Interfaces
 */
interface ApiExceptionInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return null|array
     */
    public function getErrorData(): ?array;
}
