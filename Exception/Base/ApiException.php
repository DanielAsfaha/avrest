<?php
namespace Aviatoo\Rest\Exception\Base;
use Aviatoo\Rest\Exception\Interfaces\ApiExceptionInterface;

/**
 * Class ApiException
 * @package Aviatoo\Rest\Exception
 */
abstract class ApiException extends \Exception implements ApiExceptionInterface
{
    /** @var  int $statusCode */
    private $statusCode;

    /** @var  array $errorData */
    private $errorData;

    /**
     * ApiException constructor.
     * @param int $statusCode
     * @param array $errorData
     * @param string $message
     */
    public function __construct(int $statusCode, array $errorData, string $message)
    {
        $this->statusCode = $statusCode;
        $this->errorData = $errorData;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return $this->errorData;
    }
}
