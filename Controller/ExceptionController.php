<?php
namespace Aviatoo\Rest\Controller;

/**
 * Class ExceptionController
 * @package Aviatoo\Rest\Controller
 */
class ExceptionController extends BaseController
{
    /**
     * @param \Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(\Exception $exception)
    {
        $response = $this->getControllerResponseFactory()->createWithException($exception);
        return $this->getResponseFactory()->createFromControllerResponse($response);
    }
}
