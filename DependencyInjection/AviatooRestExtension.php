<?php
namespace Aviatoo\Rest\DependencyInjection;


use Aviatoo\Rest\Types\PrimaryDateType;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class AviatooRestExtension extends Extension implements PrependExtensionInterface
{
    public function prepend(ContainerBuilder $container)
    {

        $config = ['dbal' => ["types"=>["primarydate"=> PrimaryDateType::class]]];
        $container->prependExtensionConfig("doctrine", $config);
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        // ... you'll load the files here later
        $loader = new YamlFileLoader($container,new FileLocator(__DIR__."/../Resources/config"));
        $loader->load("services.yml");
    }
}